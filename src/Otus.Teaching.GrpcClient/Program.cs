﻿// See https://aka.ms/new-console-template for more information
using Grpc.Net.Client;
using Otus.Teaching.PromoCodeFactory.WebHost.Protos;

Console.WriteLine("Testing gRPC proc-s from CustomerX service");

var channel = GrpcChannel.ForAddress("https://localhost:5001");
var client = new CustomerX.CustomerXClient(channel);

await client.SayHelloAsync(new HelloRequest() { Name = "Client X" });

var b = await client.GetCustomersAsync(new GetCustomersRequest() {});

var c = await client.CreateCustomerAsync(new CreateEditCustomerRequest()
{
    Email = "tocreate@mail.com",
    FirstName = "first",
    LastName = "last",
    PreferenceIds = { "ef7f299f-92d7-459f-896e-078ed53ef99c", "c4bda62e-fc74-4256-a956-4760b3858cbd" }
});

var d = await client.EditCustomerAsync(new CreateEditCustomerRequest()
{
    Id = c.Id,
    Email = "tocreate@mail.com",
    FirstName = "first",
    LastName = "last",
    PreferenceIds = { "76324c47-68d2-472d-abb8-33cfa8cc0c84" }
});

b = await client.GetCustomersAsync(new GetCustomersRequest() { });

var e = await client.DeleteCustomerAsync(new DeleteCustomerRequest()
{
    Id = c.Id
});

Console.WriteLine(e);