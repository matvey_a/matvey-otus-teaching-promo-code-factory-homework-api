﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Protos;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class CustomerService : CustomerX.CustomerXBase
    {
        private readonly ILogger<CustomerService> _logger;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerService(ILogger<CustomerService> logger,
                               IRepository<Customer> customerRepository,
                               IRepository<Preference> preferenceRepository)
        {
            _logger = logger;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override Task<HelloReply> SayHello(HelloRequest request, ServerCallContext context)
        {
            var helloReply = new HelloReply
            {
                Message = "Hello " + request.Name + ", you are the best!"
            };

            _logger.LogWarning("I am going to Reply hello now");

            return Task.FromResult(helloReply);
        }

        public override async Task<CustomerShortListReply> GetCustomers(GetCustomersRequest request,
                                                                  ServerCallContext context)
        {
            var res = new CustomerShortListReply();

            foreach (var customer in await _customerRepository.GetAllAsync())
            {
                res.List.Add(
                    new CustomerShortReply()
                    {
                        Email = customer.Email,
                        FirstName = customer.FirstName,
                        LastName = customer.LastName,
                        Id = customer.Id.ToString()
                    }
                    );
            }
            return res;
        }

        public override async Task<CustomerCreatedReply> CreateCustomer(CreateEditCustomerRequest request,
                                                                  ServerCallContext context)
        {
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds.Select(a => Guid.Parse(a)).ToList());

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            return new CustomerCreatedReply(){Id = customer.Id.ToString()};
        }

        public override async Task<CustomerEditReply> EditCustomer(CreateEditCustomerRequest request,
                                                             ServerCallContext context)
        {

            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                return new CustomerEditReply(){ErrorMessage = "Customer Not found" };

            var preferences = 
                await _preferenceRepository.GetRangeByIdsAsync(
                    request.PreferenceIds.Select(a => Guid.Parse(a)).ToList());

            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);
            
            return new CustomerEditReply(){ErrorMessage = ""};
        }

        public override async Task<DeleteCustomerReply> DeleteCustomer(DeleteCustomerRequest request,
                                                                 ServerCallContext context)
        {


            var customer = await _customerRepository.GetByIdAsync( Guid.Parse(request.Id) );

            if (customer == null)
                return new DeleteCustomerReply() {ErrorMessage = "Customer Not found" };

            await _customerRepository.DeleteAsync(customer);

            return new DeleteCustomerReply(){ErrorMessage = ""};
        }
    }
}
